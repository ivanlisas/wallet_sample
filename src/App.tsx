import { useEffect, useState } from 'react'
import './App.css'
import SingButton from './components/SingButton'
import WalletConnectButton from './components/WalletConnectButton'
import challengeService from './services/challengeService'
import { UserDataLogin } from './types/UserTypes'

function App() {
  const [provider, setProvider] = useState<any>(null)
  const [account, setAccount] = useState('')
  const [signature, setSignature] = useState('')
  const [error, setError] = useState('')
  const [response, setResponse] = useState<UserDataLogin>()

  // Cada vez que se genera una firma nueva le pego a la API para
  // verificar la misma, y en caso afirmativo, obtener el usuario con sus tokens
  useEffect(() => {
    const checkSignature = async () => {
      try {
        setResponse(await challengeService.checkChallenge(account, signature))
      } catch (error: any) {
        console.log(error.response)
      }
    }
    signature && checkSignature()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [signature])

  return (
    <div className="App">
      {console.log(response)}
      <WalletConnectButton setAccount={setAccount} setProvider={setProvider} />
      <p>Wallet address: {account}</p>
      <SingButton account={account} provider={provider} setSignature={setSignature} />
      <p>Signature: {signature}</p>
      <p>Json token: {response && <p style={{ lineBreak: 'anywhere' }}>{response.jsonToken}</p>}</p>
      {error && <p>error: {error}</p>}
    </div>
  )
}

export default App
