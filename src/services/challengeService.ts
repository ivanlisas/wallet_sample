import axios from 'axios'
import { UserDataLogin } from '../types/UserTypes'

class ChallengeService {
  axiosConfig = { timeout: 3000 }

  SERVER_URL = 'http://localhost:3000/app/v1'

  async getNonce(public_address: string): Promise<string> {
    return (await axios.post<any>(`${this.SERVER_URL}/auth/user/getNonce`, { public_address: public_address })).data.nonce
  }

  //
  async checkChallenge(public_address: string, signature: string): Promise<UserDataLogin> {
    return (await axios.post<UserDataLogin>(`${this.SERVER_URL}/auth/user/checkSignature`, { public_address: public_address, signature: signature }))
      .data
  }
}

const challengeService = new ChallengeService()

export default challengeService
