export type UserDataLogin = {
  user: UserDTO
  refreshToken: string
  jsonToken: string
}

export type UserDTO = {
  id: number
  username: string
  profile_pic: string
  verified: boolean
  interested: number | undefined
}
