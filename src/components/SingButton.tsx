import { useState } from 'react'
import challengeService from '../services/challengeService'

interface SingButtonProps {
  account: string
  provider: any
  setSignature: (signature: string) => void
}

const SingButton: React.FC<SingButtonProps & React.DetailedHTMLProps<React.ButtonHTMLAttributes<HTMLButtonElement>, HTMLButtonElement>> = (props) => {
  const { account, provider, setSignature, ...otherProps } = props
  const [error, setError] = useState('')

  const personalSign = async () => {
    try {
      setError('')
      const nonce = await challengeService.getNonce(account)
      provider
        .request({
          method: 'personal_sign',
          params: [nonce, account]
        })
        .then(setSignature)
    } catch (error) {
      if (error instanceof Error) {
        console.log(error.message)
        setError(error.message)
      }
    }
  }

  return (
    <div style={{ display: 'flex', flexDirection: 'column', alignItems: 'center' }}>
      <button {...otherProps} onClick={personalSign} disabled={!account}>
        Sign message
      </button>
      <strong style={{ color: 'red' }}> {error}</strong>
    </div>
  )
}

export default SingButton
