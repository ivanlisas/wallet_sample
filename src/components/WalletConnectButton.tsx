import RLogin, { RLoginButton } from '@rsksmart/rlogin'
import WalletConnectProvider from '@walletconnect/web3-provider'

interface WalletConnectButtonProps {
  setAccount: (account: string) => void
  setProvider: (provider: any) => void
}

// construct rLogin pop-up in DOM
const rLogin = new RLogin({
  // change to true to cache user's wallet choice
  providerOptions: {
    // read more about providers setup in https://github.com/web3Modal/web3modal/
    walletconnect: {
      package: WalletConnectProvider, // setup wallet connect for mobile wallet support
      options: {
        rpc: {
          31: 'https://public-node.testnet.rsk.co' // use RSK public nodes to connect to the testnet
        }
      }
    }
  },
  supportedChains: [31] // enable rsk testnet network
})

const WalletConnectButton: React.FC<WalletConnectButtonProps> = (props) => {
  const { setAccount, setProvider } = props

  // display pop up
  const connect = () =>
    rLogin.connect().then(({ provider }) => {
      // the provider is used to operate with user's wallet
      setProvider(provider)
      // request user's account
      provider.request({ method: 'eth_accounts' }).then(([account]: any) => setAccount(account))
    })

  return <RLoginButton onClick={connect}>Connect wallet</RLoginButton>
}

export default WalletConnectButton
